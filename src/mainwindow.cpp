#include "mainwindow.h"
#include "ui_mainwindow.h"

//std/stl
#include <iostream>
#include <vector>
#include <cstdint>
#include <netinet/in.h>
#include <sstream>
#include <future>
#include <thread>
using namespace std;

//boost
#include <boost/bind.hpp>
using boost::asio::ip::udp;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    m_config_socket = new QUdpSocket(this);
    int listen_port = 2224;
    bool bound = m_config_socket->bind(listen_port, QUdpSocket::ShareAddress);
    if(!bound) {
        delete m_config_socket;
        cout << "MainWindow::start_configuration_socket_listener    ";
        cout << "Failed to bind configuration socket" << endl;
        exit(1); 
    }
    ui->status_text->setText("Waiting for Config from VERSO");
    connect(m_config_socket, SIGNAL(readyRead()), this, SLOT(read_config_data()));

    // buttons
    connect(ui->button_start, SIGNAL(clicked()), this, SLOT(start_collecting()));
    connect(ui->button_pause, SIGNAL(clicked()), this, SLOT(pause_collecting()));
    connect(ui->button_stop, SIGNAL(clicked()), this, SLOT(stop_collecting()));
    connect(ui->button_verbose, SIGNAL(clicked()), this, SLOT(set_verbose()));

    initial_state();

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::initial_state()
{
    m_received_channel = -1;
    m_received_pdo = -1;
    m_received_tdo = -1;
    m_stop = false;
    m_pause = false;

    //ui->button_start->setEnabled(false);
    //ui->button_stop->setEnabled(false);
    //ui->button_verbose->setEnabled(true);
    //ui->button_pause->setEnabled(false);

    const char* dir = std::getenv("HOME");
    if(dir) {
        try {
            std::string home_dir(dir);
            if(home_dir != "") {
                ui->text_output_location->setText(QString::fromStdString(home_dir));
            }
        }
        catch(std::exception& e) {
            cout << "MainWindow::initial_state    WARNING Could not set initial directory: " << e.what() << endl;
        }
    }

}

void MainWindow::start_configuration_socket_listener()
{

    cout << "MainWindow::start_configuration_socket_listener()" << endl;


    
}

void MainWindow::read_config_data()
{
    //cout << "MainWindow::read_config_data" << endl;

    QHostAddress fromIP;
    quint16 fromPort;
    QByteArray incomingDatagram;


    while(m_config_socket->hasPendingDatagrams()) {
        incomingDatagram.resize(m_config_socket->pendingDatagramSize());
        m_config_socket->readDatagram(incomingDatagram.data(),
                    incomingDatagram.size(), &fromIP, &fromPort);

        uint32_t* config_data = (uint32_t*)incomingDatagram.data();
        size_t n_bytes = incomingDatagram.size();
        size_t n_32 = (n_bytes / 4);

        //std::cout << "  >> data from IP: " << fromIP.toString().toStdString();
        //std::cout << ",  port: " << std::dec << (unsigned)fromPort;
        //std::cout << ",  n bytes: " << std::dec << n_bytes << "  (n 32: " << (n_32) << ")";
        //std::cout << ",  data: ";
        //for(size_t i = 0; i < n_32; i++) {
        //    std::cout << std::hex << (unsigned)ntohl(config_data[i]) << " ";
        //    config_data[i] = ntohl(config_data[i]);
        //}
        //std::cout << "\n";

        if(n_32 != 8) {
            cout << "MainWindow::read_config_data    ERROR Incoming config data is not 8x32 bits long!" << endl;
            return;

        }
        uint32_t vmmid = 0xf & config_data[0];
        uint32_t pulser_dac = 0x3ff & config_data[1];
        uint32_t threshold_dac = 0x3ff & config_data[2];
        uint32_t gain = 0x1f & config_data[3];
        uint32_t peak_time = 0x1f & config_data[4];
        uint32_t tac_slope = 0x1f & config_data[5];
        uint32_t lower_32_chans = config_data[6];
        uint32_t upper_32_chans = config_data[7];
        uint64_t channel_mask = 0x0;
        channel_mask = (channel_mask | lower_32_chans);
        channel_mask |= ((uint64_t)upper_32_chans << 32);

        m_cfg.vmmid = vmmid;
        m_cfg.pulser_dac = pulser_dac;
        m_cfg.threshold_dac = threshold_dac;
        m_cfg.gain = gain;
        m_cfg.peak_time = peak_time;
        m_cfg.tac_slope = tac_slope;
        m_cfg.channel_mask = channel_mask;
        

//        m_cfg.print();

        ui->status_text->setText("Configuration Receieved");
        show_configuration();
        set_counts();

        //ui->button_start->setEnabled(true);
        //ui->button_pause->setEnabled(true);
        //ui->button_stop->setEnabled(false);

    }
}

void MainWindow::show_configuration()
{

    // channel string
    uint64_t channel_mask = m_cfg.channel_mask;
    stringstream sx;
    for(size_t i = 0; i < 64; i++) {
        uint64_t mask = (1 << i);
        if(mask & channel_mask) {
            sx << i << " ";
        }
    }
    ui->text_channels->setText(QString::fromStdString(sx.str()));
    sx.str("");
}

void MainWindow::set_counts()
{
    m_counts_map.clear();
    m_channels.clear();

    uint64_t channel_mask = m_cfg.channel_mask;
    for(size_t i = 0; i < 64; i++) {
        uint64_t mask = (1 << i);
        if(mask & channel_mask) {
            m_channels.push_back(i);
            m_counts_map[i] = 0;
        }
    }
}

void ListenerThread(std::shared_ptr<boost::asio::io_service> io_service)
{
    io_service->run();
}

void MainWindow::start_collecting()
{
    // io
    m_io_service = std::make_shared<boost::asio::io_service>();

    int channel_port = 1234;
    m_channel_socket = std::make_shared<boost::asio::ip::udp::socket>(*m_io_service,
        udp::endpoint(boost::asio::ip::address::from_string("127.0.0.1"), channel_port));

    cout << "channel socket is open ? " << m_channel_socket->is_open() << endl;

    int pdo_port = 2228;
    m_pdo_socket = std::make_shared<boost::asio::ip::udp::socket>(*m_io_service,
        udp::endpoint(boost::asio::ip::address::from_string("127.0.0.1"), pdo_port)); 
        //udp::endpoint(udp::v4(), pdo_port));
    cout << "pdo socket is open     ? " << m_pdo_socket->is_open() << endl;
    cout << "MainWindow::start_collecting()" << endl;
    m_stop = false;



//    ui->button_stop->setEnabled(true);

    //if(!m_io_service->stopped()) {
    //    m_io_service->stop();
    //    m_io_service->reset();
    //    m_io_service->stop();
    //    m_io_service->reset();
    //}
    for(int i = 0; i < 2; i++)
        m_thread_group.create_thread(boost::bind(ListenerThread, m_io_service));

    m_stop = false;
    m_pause = false;

    m_io_service->post(boost::bind(&MainWindow::receive_incoming_channels, this));
    m_io_service->post(boost::bind(&MainWindow::receive_pdo_values, this));

    //std::future<int> channel_receive = std::async(
    //        std::launch::async,
    //        &MainWindow::receive_incoming_channels,
    //        this
    //    );

    //std::future<int> adc_receive = std::async(
    //        std::launch::async,
    //        &MainWindow::receive_pdo_values,
    //        this
    //    );

    //int chan_result = channel_receive.get();
    //int adc_result = adc_receive.get();

}

void MainWindow::pause_collecting()
{
    cout << "MainWindow::pause_collecting()" << endl;
}

void MainWindow::stop_collecting()
{
    cout << "MainWindow::stop_collecting()" << endl;

    m_stop = true;

    m_channel_socket->close();
    boost::system::error_code ec;
    m_channel_socket->shutdown(boost::asio::ip::udp::socket::shutdown_both, ec);

    m_pdo_socket->close();
    m_pdo_socket->shutdown(boost::asio::ip::udp::socket::shutdown_both, ec);

    m_io_service->stop();
    m_io_service->reset();
    m_thread_group.join_all();

}

void MainWindow::set_verbose()
{

}

int MainWindow::receive_incoming_channels()
{

    cout << "MainWindow::receive_incoming_channels    ["<<std::this_thread::get_id()<< "]"<< endl;

    if(!m_stop) {
        m_channel_socket->async_receive(
            boost::asio::buffer(m_channel_buffer),
            boost::bind(&MainWindow::on_receive_channel, this,
            boost::asio::placeholders::error,
            boost::asio::placeholders::bytes_transferred)
        );
    }


    return 0;
}

int MainWindow::receive_pdo_values()
{
    cout << "MainWindow::receive_pdo_values    ["<<std::this_thread::get_id()<<"]"<<endl;

    if(!m_stop) {
        m_pdo_socket->async_receive(
            boost::asio::buffer(m_pdo_buffer),
            boost::bind(&MainWindow::on_receive_pdo, this,
            boost::asio::placeholders::error,
            boost::asio::placeholders::bytes_transferred)
        );
    }

    return 0;
}

void MainWindow::on_receive_channel(const boost::system::error_code& /*error*/,
        std::size_t n_bytes)
{
    if((int)n_bytes==0)
        receive_incoming_channels();

    //uint8_t data_in [n_bytes];
    size_t n32 = (n_bytes / 4);
    uint32_t data_in [n_bytes/4];

    std::copy(m_channel_buffer.begin(), m_channel_buffer.begin() + n_bytes, data_in);
    for(size_t i = 0; i < n_bytes/4; i++) {
        data_in[i] = ntohl(data_in[i]);
    }

    cout << "MainWindow::on_receive_channel    ["<<std::this_thread::get_id()<<"]    Channel data received! (bytes = " << n_bytes << ")" << endl;

    //cout << " > data [n_bytes = " << std::dec << n_bytes << "] : ";
    //for(size_t i = 0; i < n_bytes/4; i++) {
    //    cout << " " << std::dec << i << " " << std::hex << " " << (unsigned)data_in[i] << " (0xff = " << (unsigned)(0xff & data_in[i]) << ")";
    //}
    //cout << endl;

    if(!m_stop)
        receive_incoming_channels();

    return;
}

void MainWindow::on_receive_pdo(const boost::system::error_code& /*error*/,
        std::size_t n_bytes)
{
    cout << "MainWindow::on_recieve_pdo    [" << std::this_thread::get_id()<< "]    PDO data received!" << endl;

    //cout << " > data [n_bytes = " << std::dec << n_bytes << "] : ";
    //for(auto & pd : m_pdo_buffer)
    //    cout << std::dec << " " << (unsigned)pd;
    //cout << endl;
    if((int)n_bytes==0)
        receive_pdo_values();

    //uint8_t data_in [n_bytes];
    size_t n32 = (n_bytes / 4);
    uint32_t data_in [n_bytes/4];

    std::copy(m_pdo_buffer.begin(), m_pdo_buffer.begin() + n_bytes, data_in);
    for(size_t i = 0; i < n_bytes/4; i++) {
        data_in[i] = ntohl(data_in[i]);
    }

    cout << "MainWindow::on_receive_pdo    ["<<std::this_thread::get_id()<<"]    pdo data received! (bytes = " << n_bytes << ")  channel received? " << m_received_channel  << endl;

    if(m_received_channel >= 0)
        m_received_channel = 0;

    //cout << " > data [n_bytes = " << std::dec << n_bytes << "] : ";
    //for(size_t i = 0; i < n_bytes/4; i++) {
    //    cout << " " << std::dec << i << " " << std::hex << " " << (unsigned)data_in[i] << " (0xff = " << (unsigned)(0xff & data_in[i]) << ")";
    //}
    //cout << endl;

    if(!m_stop)
        receive_pdo_values();

    return;
}
