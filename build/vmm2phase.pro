#-------------------------------------------------
#
# project file for vmm2phase
#
# daniel.joseph.antrim@cern.ch
# April 2017
#
#-------------------------------------------------

boostinclude=/usr/local/Cellar/boost160/1.60.0/include
boostlib=/usr/local/Cellar/boost160/1.60.0/lib

linebreak="---------------------------------------------------------------"
sourcepath=../src
includepath=../include
imagepath=../images

message($$linebreak)
message("qmake sourcepath:      $$sourcepath")
message("qmake includepath:     $$includepath")
message("qmake boostinclude:    $$boostinclude")
message("qmake boostlib:        $$boostlib")
message("qmake imagepath:       $$imagepath")
message("ROOTSYS:               $$(ROOTSYS)")
message($$linebreak)

QT       += core gui
QT       += network
QT       += widgets

greaterThan(QT_MAJOR_VERSION, 4): QT += printsupport

CONFIG += console
CONFIG += declarative_debug
CONFIG += c++11

TARGET = vmm2phase

TEMPLATE = app

DEFINES += BOOST_ALL_NO_LIB

linux {
    QMAKE_RPATHDIR += $$boostlib
    QMAKE_RPATHDIR += ./objects
}

INCLUDEPATH += $(ROOTSYS)/include
LIBS += -L$(ROOTSYS)/lib -lCore -lCint -lRIO -lNet \
       -lHist -lGraf -lGraf3d -lGpad -lTree \
       -lRint -lPostscript -lMatrix -lPhysics \
       -lGui -lMathCore #-lRGL -lMathCore

LIBS +=  -L$$boostlib -lboost_thread-mt -lboost_filesystem-mt  -lboost_system-mt -lboost_chrono-mt -lboost_atomic-mt

LIBS += -L./objects -lMylib

INCLUDEPATH += $$includepath
DEPENDPATH  += $$includepath
INCLUDEPATH += $$includepath/mapping/
DEPENDPATH  += $$includepath/mapping/
INCLUDEPATH += $$includepath/monitoring/
DEPENDPATH  += $$includepath/monitoring/
INCLUDEPATH += $$includepath/configuration/
DEPENDPATH  += $$includepath/configuration/
INCLUDEPATH += $$includepath/dataflow/
DEPENDPATH  += $$includepath/dataflow/
INCLUDEPATH += $$boostinclude
DEPENDPATH  += $$boostinclude

OBJECTS_DIR += ./objects/
MOC_DIR     += ./moc/
RCC_DIR     += ./rcc/
UI_DIR      += ./ui/


SOURCES += $$sourcepath/main.cpp\
           $$sourcepath/mainwindow.cpp

HEADERS  += $$includepath/mainwindow.h\
            $$includepath/configuration_holder.h

FORMS    += $$sourcepath/mainwindow.ui

RESOURCES += \
    $$imagepath/icons.qrc
