#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QUdpSocket>

#include <map>
#include <vector>
#include <atomic>
#include "configuration_holder.h"
#include <memory>

//boost
#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <boost/array.hpp>

#define MAX_UDP_LEN 2500
typedef boost::array<uint32_t, MAX_UDP_LEN> data_array_t;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    void initial_state();
    void start_configuration_socket_listener();
    void show_configuration();
    void set_counts();

    int receive_incoming_channels();
    void on_receive_channel(const boost::system::error_code& error,
                std::size_t n_bytes /*bytes transferred*/);

    int receive_pdo_values();
    void on_receive_pdo(const boost::system::error_code& error,
                std::size_t n_bytes /*bytes transferred*/);


    QUdpSocket* m_config_socket;
    ConfigurationHolder m_cfg;
    std::map<int, int> m_counts_map;
    std::vector<int> m_channels;

    std::atomic<int> m_received_channel;
    std::atomic<int> m_received_pdo;
    std::atomic<int> m_received_tdo;
    std::atomic<bool> m_stop;
    std::atomic<bool> m_pause;


    // io
    boost::thread_group m_thread_group; 

    std::shared_ptr<boost::asio::io_service> m_io_service;

    std::shared_ptr<boost::asio::ip::udp::socket> m_channel_socket;
    boost::asio::ip::udp::endpoint m_channel_endpoint;
    data_array_t m_channel_buffer;

    std::shared_ptr<boost::asio::ip::udp::socket> m_pdo_socket;
    boost::asio::ip::udp::endpoint m_pdo_endpoint;
    data_array_t m_pdo_buffer;

    
    

public slots :
    void read_config_data();

    void start_collecting();
    void pause_collecting();
    void stop_collecting();
    void set_verbose();

signals :
    void move_to_next();

};

#endif // MAINWINDOW_H
