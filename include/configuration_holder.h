#ifndef NSW_CONFIGURATION_HOLDER
#define NSW_CONFIGURATION_HOLDER

#include <cstdint>
#include <bitset>
#include <iostream>
using namespace std;

class ConfigurationHolder
{
    public :
        ConfigurationHolder()
        {
            vmmid = 0;
            pulser_dac = 0;
            threshold_dac = 0;
            gain = 0;
            peak_time = 0;
            tac_slope = 0;
            uint64_t channel_mask = 0;
        }

        void print()
        {
            cout << std::dec;
            cout << "ConfigurationHolder::print    vmmid = " << vmmid;
            cout << ", pulser_dac = " << pulser_dac;
            cout << ", threshold_dac = " << threshold_dac;
            cout << ", gain = " << gain;
            cout << ", peak_time = " << peak_time;
            cout << ", tac_slope = " << tac_slope;
            cout << "\nchannel mask = " << std::bitset<64>(channel_mask) << endl; 
        }

        uint32_t vmmid;
        uint32_t pulser_dac;
        uint32_t threshold_dac;
        uint32_t gain;
        uint32_t peak_time;
        uint32_t tac_slope;
        uint64_t channel_mask;
};

#endif
